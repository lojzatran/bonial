package com.bonial

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.spock.IntegrationSpec

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
class ShoppingCartServiceSpec extends IntegrationSpec {

    def shoppingCartService

    def "when user add item to shopping cart, it should be there"() {
        setup: "we create test item, user and mock spring security service"
        Item testItem = new Item(name: "testItem")
        testItem.save(flush: true)

        User testUser = new User(username: "test", password: "test")
        testUser.save(flush: true)

        def securityService = Mock(SpringSecurityService)
        securityService.getCurrentUserId() >> testUser.id

        shoppingCartService.springSecurityService = securityService

        when: "we add a new item into nonexisting cart"
        shoppingCartService.addItemToShoppingCart(testItem.id)
        ShoppingCart cart = ShoppingCart.where {
            user.id == testUser.id
        }.find()

        then: "it should create new cart and add the item into it"
        cart
        cart.itemSet.contains(testItem)

        when: "add another item into shopping cart"
        shoppingCartService.addItemToShoppingCart(testItem.id)
        cart = ShoppingCart.where {
            user.id == testUser.id
        }.find()

        then:
        cart
        cart.itemSet.contains(testItem)
    }

    def "when user remove item from shopping cart, it should be removed"() {
        setup: "we create test item, user and mock spring security service"
        Item testItem = new Item(name: "testItem")
        testItem.save(flush: true)

        User testUser = new User(username: "test", password: "test")
        testUser.save(flush: true)

        def securityService = Mock(SpringSecurityService)
        securityService.getCurrentUserId() >> testUser.id

        shoppingCartService.springSecurityService = securityService

        and: "we add a new item into nonexisting cart"
        shoppingCartService.addItemToShoppingCart(testItem.id)
        ShoppingCart cartBefore = ShoppingCart.where {
            user.id == testUser.id
        }.find()

        and: "detach from the current persistence context so any further changes wont reflect"
        cartBefore.discard()

        when: "we remove an item"
        shoppingCartService.removeItemFromShoppingCart(testItem.id)
        ShoppingCart cartAfter = ShoppingCart.where {
            user.id == testUser.id
        }.find()

        then: "after removing the item, there should be one less in the shopping cart"
        cartBefore.itemSet.size() == cartAfter.itemSet.size() + 1

    }
}
