<%--
  Created by IntelliJ IDEA.
  User: lojzatran
  Date: 19/07/15
  Time: 20:01
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>My shopping list</title>
    </head>

    <body>
        <h1 class="text-center">My shopping list</h1>
        <g:if test="${!shoppingCart.itemSet.isEmpty()}">
            <div class="col-sm-offset-3 col-sm-6">
                <table id="cart" class="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style="width:50%">Item</th>
                            <th style="width:10%">Color</th>
                            <th style="width:10%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${shoppingCart.itemSet}" var="item">
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <h4 class="nomargin">${item.name}</h4>
                                        </div>
                                    </div>
                                </td>
                                <td>${item.color}</td>
                                <td class="actions">
                                    <g:form action="removeItemFromShoppingCart"
                                            onsubmit="return confirm('Are you sure?');"
                                            params="[id: item.id]" method="post">
                                        <button type="submit" class="btn btn-danger btn-sm">
                                            <i class="fa fa-trash-o"></i>
                                        </button>
                                    </g:form>
                                </td>
                            </tr>
                        </g:each>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <g:link controller="item" action="list" class="btn btn-warning">
                                    <i class="fa fa-angle-left"></i> Continue Shopping
                                </g:link>
                            </td>
                            <td></td>
                            <td><a href="#" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i>
                            </a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </g:if>
        <g:else>
            <p>
                You don't have anything in your shopping list. Go to
                <g:link controller="item" action="list">Item page</g:link> and add something.
            </p>
        </g:else>

    </body>
</html>