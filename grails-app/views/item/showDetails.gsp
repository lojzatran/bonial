<%--
  Created by IntelliJ IDEA.
  User: lojzatran
  Date: 19/07/15
  Time: 14:14
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>${item.name}</title>
    </head>

    <body>
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Name</label>

                <div class="col-sm-10">
                    <p class="form-control-static">${item.name}</p>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Available</label>

                <div class="col-sm-10">
                    <p class="form-control-static">${item.isReserved() ? "NO" : "YES"}</p>
                </div>
            </div>


            <div class="form-group">
                <g:form controller="shoppingCart" action="addItemToShoppingCart" params="[itemId: item.id]">
                    Add to shopping list
                </g:form>
            </div>
        </div>
    </body>
</html>