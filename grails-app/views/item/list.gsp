<%--
  Created by IntelliJ IDEA.
  User: lojzatran
  Date: 19/07/15
  Time: 13:34
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>Item list</title>
    </head>

    <body>
        <div class="col-sm-8 col-sm-offset-2">
            <h1>Item list</h1>

            <div>
                <sec:ifLoggedIn>
                    <g:link controller="shoppingCart" action="show" class="pull-left"
                            params="[username: sec.loggedInUserInfo(field: 'username')]">
                        Go to my shopping cart
                    </g:link>
                    <g:form controller="logout" action="index" method="post">
                        <g:submitButton name="Log out" class="pull-right btn btn-link"/>
                    </g:form>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <g:link controller="login" action="index" class="pull-right">Please log in to continue</g:link>
                </sec:ifNotLoggedIn>
            </div>

            <div class="col-sm-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Color</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${itemList}" var="item">
                            <tr>
                                <td <g:if test="${item.shoppingCart}">style="text-decoration: line-through;"</g:if>>
                                    <g:link controller="item" action="showDetails"
                                            id="${item.id}">
                                        ${item.name}
                                    </g:link>

                                </td>
                                <td>${item.color}</td>
                                <td>
                                    <sec:ifLoggedIn>
                                        <g:form controller="shoppingCart" action="addItemToShoppingCart"
                                                params="[itemId: item.id]" method="post">
                                            <button type="submit" class="btn btn-primary"
                                                    <g:if test="${item.shoppingCart}">disabled="disabled"</g:if>>
                                                Add to shopping list
                                            </button>
                                        </g:form>
                                    </sec:ifLoggedIn>
                                </td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
