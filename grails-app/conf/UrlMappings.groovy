class UrlMappings {

    static mappings = {
        "/$username"(controller: "shoppingCart", action: "show")

        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "item", action: "list")
        "500"(view: '/error')
    }
}
