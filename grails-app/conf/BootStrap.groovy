import com.bonial.*
import grails.util.Environment

class BootStrap {

    def init = { servletContext ->

        if (Environment.current == Environment.DEVELOPMENT) {
            User user = new User(username: "user", password: "password1").save(flush: true)
            Role userRole = new Role(authority: "ROLE_USER").save(flush: true)
            new UserRole(user: user, role: userRole).save(flush: true)

            User admin = new User(username: "admin", password: "password1").save(flush: true)
            Role adminRole = new Role(authority: "ROLE_ADMIN").save(flush: true)
            new UserRole(user: admin, role: adminRole).save(flush: true)


            Item tShirt = new Item(name: "T-shirt", itemCode: UUID.randomUUID().toString(), color: Color.BLUE)
            Item jacket = new Item(name: "Jacket", itemCode: UUID.randomUUID().toString(), color: Color.RED)
            tShirt.save()
            jacket.save()
        }

    }

    def destroy = {
    }
}
