package com.bonial

import grails.plugin.springsecurity.annotation.Secured

/**
 * Controller for {@link Item}
 *
 * @author lojzatran
 */
@Secured(['IS_AUTHENTICATED_ANONYMOUSLY'])
class ItemController {

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        List<Item> itemList = Item.findAll()
        [itemList: itemList]
    }

    def showDetails(Long id) {
        Item item = Item.findById(id)
        [item: item]
    }
}
