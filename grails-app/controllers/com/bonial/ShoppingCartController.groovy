package com.bonial

import grails.plugin.springsecurity.annotation.Secured
import org.springframework.http.HttpStatus

/**
 * Controller for manipulation with {@link ShoppingCart}
 */
@Secured(['isAuthenticated()'])
class ShoppingCartController {

    static allowedMethods = [addItemToShoppingCart     : 'POST',
                             removeItemFromShoppingCart: ['POST', 'DELETE']]

    def shoppingCartService

    def springSecurityService

    def show(String username) {
        User currentUser = springSecurityService.currentUser
        if (username == currentUser.username) {
            ShoppingCart shoppingCart = shoppingCartService.findCartForCurrentUser().
                    orElse(new ShoppingCart())
            [shoppingCart: shoppingCart]
        } else {
            flash.errorMessage = "You can't access this URL."
            render view: "/error", status: HttpStatus.NOT_FOUND
        }
    }

    def addItemToShoppingCart(Long itemId) {
        shoppingCartService.addItemToShoppingCart(itemId)
        User currentUser = springSecurityService.currentUser
        flash.successMessage = "You have added item to your shopping list"
        redirect url: "/${currentUser.username}"
    }

    def removeItemFromShoppingCart(Long id) {
        shoppingCartService.removeItemFromShoppingCart(id)
        User currentUser = springSecurityService.currentUser
        flash.successMessage = "You have successfully removed item from your shopping list"
        redirect url: "/${currentUser.username}"
    }
}
