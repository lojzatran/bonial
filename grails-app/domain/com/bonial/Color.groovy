package com.bonial

/**
 * Color of an item
 *
 * @author lojzatran
 */
enum Color {
    RED, GREEN, WHITE, BLACK, BLUE
}