package com.bonial

/**
 * Item in the shop
 */
class Item {

    String name

    String itemCode

    Color color

    static belongsTo = [shoppingCart: ShoppingCart]

    static mapping = {
        version false
    }

    static constraints = {
        shoppingCart nullable: true
        itemCode nullable: true
        color nullable: true
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Item item = (Item) o

        if (id != item.id) return false

        return true
    }

    int hashCode() {
        return (id != null ? id.hashCode() : 0)
    }

    public boolean isReserved() {
        return !shoppingCart
    }
}
