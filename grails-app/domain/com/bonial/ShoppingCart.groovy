package com.bonial

/**
 * Shopping cart where you can put the items you want to buy
 */
class ShoppingCart {

    Set<Item> itemSet = [] as Set

    Date lastUpdated

    User user

    static hasMany = [itemSet: Item]

    static constraints = {
        user unique: false
    }

    static mapping = {
        version false
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        ShoppingCart that = (ShoppingCart) o

        if (id != that.id) return false

        return true
    }

    int hashCode() {
        return (id != null ? id.hashCode() : 0)
    }
}
