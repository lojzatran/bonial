package com.bonial
/**
 * Shopping cart service
 *
 * @author lojzatran
 */
class ShoppingCartService {

    def springSecurityService

    /**
     * Add an item to current user's shopping cart.
     * If user doesn't have one, a new cart is created for him.
     *
     * @param itemId ID of the item
     * @return user's shopping cart
     */
    public void addItemToShoppingCart(Long itemId) {
        Long currentUserId = springSecurityService.currentUserId as Long
        Item item = Item.findById(itemId)
        if (item) {
            ShoppingCart shoppingCart
            Optional<ShoppingCart> shoppingCartOpt = findCartForCurrentUser()
            if (shoppingCartOpt.isPresent()) {
                shoppingCart = shoppingCartOpt.get()
            } else {
                shoppingCart = new ShoppingCart(user: User.load(currentUserId))
            }
            shoppingCart.addToItemSet(item)
            shoppingCart.save()
        }
    }

    /**
     * Find a shopping cart for current user
     */
    public Optional<ShoppingCart> findCartForCurrentUser() {
        Long currentUserId = springSecurityService.currentUserId as Long
        ShoppingCart shoppingCart = ShoppingCart.where {
            user.id == currentUserId
        }.find()
        return Optional.ofNullable(shoppingCart)
    }

    /**
     * Remove an item from current user's shopping cart
     * @param itemId ID of the item to remove
     */
    public void removeItemFromShoppingCart(Long itemId) {
        Optional<ShoppingCart> shoppingCartOpt = findCartForCurrentUser()
        if (shoppingCartOpt.isPresent()) {
            ShoppingCart shoppingCart = shoppingCartOpt.get()
            Item itemToRemove = shoppingCart.itemSet.find { it.id == itemId }
            shoppingCart.removeFromItemSet(itemToRemove)
            shoppingCart.save(flush: true)
        }
    }
}
