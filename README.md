# The Task#

You should create a web application that fulfills the requirements listed below. The focus should be on functionality and clean code, not on the UI design.

The application should then be provided via email, github or bitbucket or any other appropriate way. Of course, this should include the source code as well as a little documentation on how to setup and use the application. It would be nice if you can provide a demo video on how the application works (You can use CamStudio [open source] or any other screen recorder tool to record the demo).


## Functionality Summary ##
MUSHOPL is a simple shopping list system that allows users to manage their shopping list. Each user has one personal shopping list where he can add entries to after signing up to the system.

## Required functionality ##
### Login/Logout ###
One shopping-list per user and it should only be visible for the user himself/herself

URL looks like: http://hostName/applicationName/<username>/

The user should be able to add items to his/her shopping-list and view/list  his/her shopping-list

### Must-Haves ###
An adequate amount of (inline) documentation should be provided

At least the backend parts should be implemented in Groovy or Java 

Application should use some embedded in-memory database (e.g. http://hsqldb.org/,   http://www.h2database.com, http://docs.oracle.com/javadb/ , ...)
Application should either be a Grails-app or use a Gradle script and an embedded servlet-container (e.g. Jetty or an embedded Tomcat, see http://www.javacodegeeks.com/2013/04/simple-gradle-web-application.html / http://codetutr.com/wp-content/uploads/2013/03/basic-web.zip for a sample)

The application should come with some basic test data


# Solution #

The app is based on Grails 2.5.0. It uses in-memory database HSQL, which is default in Grails. 

To run the app, you have two options. Both options assumes you have Java 8 installed in your computer:
* Install Grails framework and set paths. Then just run grails run-app from the solution root directory in order to start the app.
* Run a standalone web app using this command (without quotes) "java -jar mushopl-standalone.jar". This standalone app includes Tomcat server and also will start with some basic data for testing.

In the app, you can use this user for testing: username: user, password: password1